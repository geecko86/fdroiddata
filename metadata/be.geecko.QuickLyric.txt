AntiFeatures:Tracking
Categories:Multimedia,Internet
License:GPLv3
Web Site:https://github.com/geecko86/QuickLyric/blob/HEAD/README.md
Source Code:https://github.com/geecko86/QuickLyric
Issue Tracker:https://github.com/geecko86/QuickLyric/issues

Auto Name:QuickLyric
Summary:Fetch and display song lyrics
Description:
QuickLyric is an android app that fetches your lyrics for you almost instantly.
Its main perks are that it doesn't have ads, its search engine, the downloading
of lyrics and some handy settings. All in all: it is simply meant to be handy.

While most lyrics apps require you to type the name of the song you're
searching for or annoy you with tedious notifications or ads every time the
song changes, QuickLyric automatically detects what song is playing on 
your device and instantly delivers the lyrics to you. 

On top of that, the design mimics the Google Play apps, making it a real 
pleasure to use.

===Features===
* A slick drawer menu.
* '''Lyrics''': Shows the lyrics of the song you're listening to at the moment.<br />In the action bar you'll find buttons to update the lyrics, to download the<br />lyrics on your device (for offline usage) and to share the URL to a friend.
* '''Local Storage''': In this screen, you can see a list of the lyrics<br />you've chosen to download.
* '''Search''': Search and find lyrics.
* '''Settings''': QuickLyric offers a minimal amount of settings to avoid<br />an overwhelming amount of choices. 3 choices are offered to the user:<br />whether transitions will be animated, whether the app should try to find<br />lyrics for tracks that are longer than 20 minutes (those are presumably<br />podcasts and not songs) and whether to automatically update the lyrics<br />when the song has changed, without having to press the refresh button in<br />the action bar. In this screen, you'll also find the "About" info.
* '''System Integration''': Access the lyrics you want after you've<br />identified a song with Shazam or Soundhound via the share button.<br/>Open URLs from AZLyrics, LyricsNMusic and LyricWikia directly with the app.
* '''NFC''': Share lyrics with your friends via Android Beam.
* '''More than 10 languages supported''': Including German, Greek,<br />Spanish, French, Italian, Japanese, Dutch, Polish, Russian, Turkish & English.

===Notes===
QuickLyric uses [http://acra.ch ACRA] to track crashes, causing the app to have
the tracking AntiFeature. <br/>The NFC permission is used to share lyrics via 
Android Beam.

===Credits===
Credits are due to : Last.FM, Roman Nurik for his scroll tricks, 
<br />Ficus Kirkpatrick for Volley, ShowcaseView.

===Screenshots===
[https://i.imgur.com/bKq0GLW.png Screenshot1]
[https://i.imgur.com/bEdjfIn.png Screenshot2]
[https://i.imgur.com/RtIdK24.png Screenshot3]
[https://i.imgur.com/dXlxpmJ.png Screenshot4]

Repo Type:git
Repo:https://github.com/geecko86/QuickLyric

Build:1.0,1
    commit=29bf3043c26e8e03b2617eaa0f1962eaee02923c
    subdir=QuickLyric
    gradle=yes
    rm=QuickLyric/libs/*
    prebuild=sed -i -e '/volley/d' build.gradle && \
        sed -i -e "/showcaseview/acompile 'com.mcxiaoke.volley:library:1.0.6'" build.gradle

Build:1.0,2
    commit=1.0
    subdir=QuickLyric
    gradle=yes
    rm=QuickLyric/libs/*
    prebuild=sed -i -e '/volley/d' build.gradle && \
        sed -i -e "/showcaseview/acompile 'com.mcxiaoke.volley:library:1.0.6'" build.gradle

Auto Update Mode:None
Update Check Mode:Tags
Current Version:1.0
Current Version Code:2

